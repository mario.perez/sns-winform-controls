﻿namespace PlayGroundProyect
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.snsTextBox1 = new basicControls.SnsTextBox();
            this.snsTextBox2 = new basicControls.SnsTextBox();
            this.snsComboBox1 = new basicControls.SnsComboBox();
            this.maskedTextBox1 = new System.Windows.Forms.MaskedTextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.button2 = new System.Windows.Forms.Button();
            this.snsFinderTextBox2 = new basicControls.SnsFinderTextBox();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // snsTextBox1
            // 
            this.snsTextBox1.ForeColor = System.Drawing.Color.Gray;
            this.snsTextBox1.Location = new System.Drawing.Point(109, 58);
            this.snsTextBox1.Name = "snsTextBox1";
            this.snsTextBox1.PlaceHolderText = "Ejemplo";
            this.snsTextBox1.Size = new System.Drawing.Size(100, 20);
            this.snsTextBox1.TabIndex = 0;
            this.snsTextBox1.Text = "Ejemplo";
            // 
            // snsTextBox2
            // 
            this.snsTextBox2.ForeColor = System.Drawing.Color.Gray;
            this.snsTextBox2.Location = new System.Drawing.Point(3, 32);
            this.snsTextBox2.Name = "snsTextBox2";
            this.snsTextBox2.PlaceHolderText = "Ejemplo2";
            this.snsTextBox2.Size = new System.Drawing.Size(100, 20);
            this.snsTextBox2.TabIndex = 1;
            this.snsTextBox2.Text = "Ejemplo2";
            // 
            // snsComboBox1
            // 
            this.snsComboBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.snsComboBox1.FormattingEnabled = true;
            this.snsComboBox1.IsBussy = false;
            this.snsComboBox1.Location = new System.Drawing.Point(3, 3);
            this.snsComboBox1.Name = "snsComboBox1";
            this.snsComboBox1.PlaceHolderText = null;
            this.snsComboBox1.Size = new System.Drawing.Size(390, 21);
            this.snsComboBox1.TabIndex = 2;
            this.snsComboBox1.Resize += new System.EventHandler(this.snsComboBox1_Resize);
            // 
            // maskedTextBox1
            // 
            this.maskedTextBox1.Location = new System.Drawing.Point(224, 32);
            this.maskedTextBox1.Name = "maskedTextBox1";
            this.maskedTextBox1.Size = new System.Drawing.Size(100, 20);
            this.maskedTextBox1.TabIndex = 3;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(399, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 20);
            this.button1.TabIndex = 4;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(3, 58);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(100, 23);
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBar1.TabIndex = 5;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Controls.Add(this.snsComboBox1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.button1, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(496, 26);
            this.tableLayoutPanel1.TabIndex = 6;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(330, 32);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 7;
            this.button2.Text = "limpiar";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // snsFinderTextBox2
            // 
            this.snsFinderTextBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(203)))), ((int)(((byte)(250)))));
            this.snsFinderTextBox2.FilterTypes = null;
            this.snsFinderTextBox2.Location = new System.Drawing.Point(3, 101);
            this.snsFinderTextBox2.MyHeaders = null;
            this.snsFinderTextBox2.Name = "snsFinderTextBox2";
            this.snsFinderTextBox2.PlaceHolderText = null;
            this.snsFinderTextBox2.Size = new System.Drawing.Size(100, 20);
            this.snsFinderTextBox2.TabIndex = 9;
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.listView1.Location = new System.Drawing.Point(109, 84);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(284, 165);
            this.listView1.TabIndex = 10;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Items";
            this.columnHeader1.Width = 100;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "nombres";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(496, 261);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.snsFinderTextBox2);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.maskedTextBox1);
            this.Controls.Add(this.snsTextBox2);
            this.Controls.Add(this.snsTextBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private basicControls.SnsTextBox snsTextBox1;
        private basicControls.SnsTextBox snsTextBox2;
        private basicControls.SnsComboBox snsComboBox1;
        private System.Windows.Forms.MaskedTextBox maskedTextBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button button2;
        private basicControls.SnsFinderTextBox snsFinderTextBox2;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
    }
}

