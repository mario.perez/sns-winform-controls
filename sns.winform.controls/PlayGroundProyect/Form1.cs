﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PlayGroundProyect
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.snsComboBox1.PlaceHolderText = "Combo de Ejemplo";
            snsComboBox1.Items.Add("uno");
            snsComboBox1.Items.Add("dos");
            snsComboBox1.Items.Add("tres");

            //=============EJEMPLO DE INSTANCIA PARA SNSFINDERTEXTBOX===============
            var heads = new List<ColumnHeader>();
            heads.Add(new ColumnHeader() { Text = "Items", Width = 200 });
            snsFinderTextBox2.MyHeaders = heads;
            snsFinderTextBox2.LoadMethods
                (n =>
                    {
                        string resp = n.FirstOrDefault();
                        return resp;
                    },
                 n =>
                    {
                        string resp = n.FirstOrDefault();
                        return resp;
                    },
                 n =>
                     {
                         
                     },
                 (out List<string[]>  n, RunWorkerCompletedEventArgs m) =>
                 {
                     List<string[]> resp = new List<string[]>();
                     var UNO = new List<string>();
                     UNO.Add("UNO");

                     var DOS = new List<string>();
                     DOS.Add("DOS");
                     
                     var TRES = new List<string>();
                     TRES.Add("TRES");

                     resp.Add(UNO.ToArray());
                     resp.Add(DOS.ToArray());
                     resp.Add(TRES.ToArray());

                     n = resp;
                 }
                );
            snsFinderTextBox2.FilterTypes = new List<string>() { "Codigo","Nombre","Telefono" };
            snsFinderTextBox2.GettingResult += SnsFinderTextBox2_GettingResult;
            //=============FIN DE EJEMPLO DE INSTANCIA PARA SNSFINDERTEXTBOX========
        }

        private void SnsFinderTextBox2_GettingResult(object sender, EventArgs e)
        {
            MessageBox.Show((string)snsFinderTextBox2.Result);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            snsComboBox1.IsBussy = !snsComboBox1.IsBussy;
        }

        private void snsComboBox1_Resize(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            snsTextBox2.Text = string.Empty;
        }
    }
}
