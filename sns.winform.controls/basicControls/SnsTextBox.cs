﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;

namespace basicControls
{
    public class SnsTextBox:MaskedTextBox
    {
        private string _placeHolderText;
        System.Drawing.Color DefaultColor;
        private bool _isHintShowed;
        private Dictionary<InputType, string> _masks;
        private ToolTip _ttMessage = new ToolTip();
        
        public SnsTextBox():base()
        {
            LoadTasks();

        }
        public string PlaceHolderText
        {
            get
            {
                return _placeHolderText;
            }
            set
            {
                _placeHolderText = value;
                LoadHint(_placeHolderText);
            }
        }

        private void LoadHint(string PlaceHolderText)
        {
            if (!string.IsNullOrWhiteSpace(PlaceHolderText))
            {
                DefaultColor = this.ForeColor;
                _isHintShowed = true;
                // Add event handler for when the control gets focus
                this.GotFocus += (object sender, EventArgs e) =>
                {
                    this.Text = _isHintShowed?String.Empty:this.Text;
                    this.ForeColor = DefaultColor;
                    _isHintShowed = false;
                };
                // add event handling when focus is lost
                this.LostFocus += (Object sender, EventArgs e) =>
                {
                    if (String.IsNullOrEmpty(this.Text) || this.Text == PlaceHolderText)
                    {
                        this.ForeColor = System.Drawing.Color.Gray;
                        this.Text = PlaceHolderText;
                        _isHintShowed = true;

                    }
                    else
                    {
                        this.ForeColor = DefaultColor;
                        _isHintShowed = false;
                    }
                };
                
                this.TextChanged += (object sender, EventArgs e)=>
                {
                    if(!this.Focused)
                    {
                        if (String.IsNullOrEmpty(this.Text) || this.Text == PlaceHolderText)
                        {
                            this.ForeColor = System.Drawing.Color.Gray;
                            this.Text = PlaceHolderText;
                            _isHintShowed = true;

                        }
                        else
                        {
                            this.ForeColor = DefaultColor;
                            _isHintShowed = false;
                        }
                    }
                    else
                    {
                        this.Text = _isHintShowed ? String.Empty : this.Text;
                        this.ForeColor = DefaultColor;
                        _isHintShowed = false;
                    }
                };
                
                if (!string.IsNullOrEmpty(PlaceHolderText))
                {
                    // change style   
                    this.ForeColor = System.Drawing.Color.Gray;
                    // Add text
                    this.Text = PlaceHolderText;
                }
            }
        }
        

        private void LoadTasks()
        {
            _masks = new Dictionary<InputType, string>();
            _masks.Add(InputType.None, "");
            _masks.Add(InputType.Money, @"$\d{9}.00");
            _masks.Add(InputType.Email, @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            _masks.Add(InputType.Number, @"#");
            _masks.Add(InputType.Phone, @"(?\d{3})?-? *\d{4}-? *-?\d{4}");
        }
        
        private void LoadInputType(InputType Type)
        {
            Mask = _masks[Type];
            this.MaskInputRejected += (object sender, MaskInputRejectedEventArgs e) =>
            {
                _ttMessage.ToolTipTitle = "Invalid Input";
                _ttMessage.Show("We're sorry, but only digits (0-9) are allowed in dates.", this, this.Location, 5000);
            };

        }



    }
}
