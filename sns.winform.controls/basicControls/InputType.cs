﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace basicControls
{
    public enum InputType
    {
        None,
        Number,
        Money,
        Email,
        Phone
    }
}
