﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;

namespace basicControls
{
    public class SnsFinderTextBox:SnsTextBox
    {
        private object _result;
        private DelegateFinder_Ok _finder_justok;
        private List<string> _filterTypes;
        public event System.EventHandler GettingResult;

        public List<ColumnHeader> MyHeaders { get; set; }
        
        public object Result
        {
            get { return _result; }
        }

        public List<string> FilterTypes { get => _filterTypes; set => _filterTypes = value; }

        public SnsFinderTextBox(): base()
        {
            BackColor = System.Drawing.Color.FromArgb(200, 203, 250);            
        }

        public void LoadMethods(DelegateFinder_Ok finder_justok, DelegateFinder_Ok finder_ok, DelegateFinder_DoWork finder_DoWork, DelegateFinder_RunWorkerCompleted finder_RunWorkerCompleted)
        {
            _finder_justok = finder_justok;
            this.KeyDown += (object sender, KeyEventArgs e) =>
            {
                if (e.KeyCode == Keys.B && e.Control)
                {
                    frmFinder _finder = new frmFinder(MyHeaders);
                    _finder.FilterTypes = _filterTypes;
                    _finder.LoadMethods(finder_ok, finder_DoWork, finder_RunWorkerCompleted);
                    _finder.ShowDialog();
                    _result = _finder.Result;
                    GettingResult.Invoke(this, new EventArgs());
                    Text = _finder.StrResult;
                }
                if (e.KeyCode == Keys.Enter)
                {
                    List<string> tempString = new List<string>();
                    tempString.Add(this.Text);
                    _result = finder_justok(tempString.ToArray());
                    GettingResult.Invoke(this, new EventArgs());
                    SendKeys.Send("{TAB}");
                }
            };
        }

    }
}
