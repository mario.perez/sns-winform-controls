﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;


namespace basicControls
{

    public class SnsComboBox : ComboBox
    {
        private string _placeHolderText;
        System.Drawing.Color DefaultColor;
        private bool _isHintShowed;
        private ProgressBar _progressBar;

        public SnsComboBox():base()
        {
            LoadProgressBar();
            
        }
        

        public bool IsBussy
        {
            set
            {
                
                _progressBar.Visible = value;
                this.Enabled = !value;
                
            }
            get { return _progressBar.Visible; }
        }

        public string PlaceHolderText
        {
            get
            {
                return _placeHolderText;
            }
            set
            {
                _placeHolderText = value;
                LoadHint(_placeHolderText);
            }
        }

        private void LoadHint(string PlaceHolderText)
        {
            if (!string.IsNullOrWhiteSpace(PlaceHolderText))
            {
                DefaultColor = this.ForeColor;
                _isHintShowed = true;
                // Add event handler for when the control gets focus
                this.GotFocus += (object sender, EventArgs e) =>
                {
                    this.Text = _isHintShowed ? String.Empty : this.Text;
                    this.ForeColor = DefaultColor;
                    _isHintShowed = false;
                };
                // add event handling when focus is lost
                this.LostFocus += (Object sender, EventArgs e) =>
                {
                    if (String.IsNullOrEmpty(this.Text) || this.Text == PlaceHolderText)
                    {
                        this.ForeColor = System.Drawing.Color.Gray;
                        this.Text = PlaceHolderText;
                        _isHintShowed = true;

                    }
                    else
                    {
                        this.ForeColor = DefaultColor;
                        _isHintShowed = false;
                    }
                };

                if (!string.IsNullOrEmpty(PlaceHolderText))
                {
                    // change style   
                    this.ForeColor = System.Drawing.Color.Gray;
                    // Add text
                    this.Text = PlaceHolderText;
                }
            }
        }

        private void LoadProgressBar()
        {
            _progressBar = new ProgressBar();
            _progressBar.Location = this.Location;
            _progressBar.Name = "_progressBar";
            _progressBar.TabIndex = 20;
            _progressBar.Size = new System.Drawing.Size(this.Size.Width - 20, Size.Height); 

            _progressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            _progressBar.Visible = false;
            this.Resize += (object sender, EventArgs e) =>
            {
                _progressBar.Size = new System.Drawing.Size(this.Size.Width - 20, Size.Height);
            };
            this.Controls.Add(_progressBar);
            
        }


    }
}
