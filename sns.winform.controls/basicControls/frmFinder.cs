﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace basicControls
{
    public partial class frmFinder : Form
    {
        const string _strWrongConfig = "El control no fue inicializado correctamente. \n Contacte a su proveedor.";
        private object _result;
        private string _strResult;
        private List<ColumnHeader> _myHeaders;


        private List<string[]> _items;
        private List<string> _filterTypes;
        private Dictionary<string, string> _filters;
        
        private DelegateFinder_Ok _finder_Ok;
        private DelegateFinder_DoWork _finder_DoWork;
        private DelegateFinder_RunWorkerCompleted _finder_RunWorkerCompleted;

        public frmFinder(List<ColumnHeader> myHeaders)
        {
            _myHeaders = new List<ColumnHeader>();

            foreach(var head in myHeaders)
            {
                _myHeaders.Add(new ColumnHeader() { Name = head.Name, Text = head.Text, Width = head.Width });
            }
            InitializeComponent();
            lstItems.Clear();
            LoadHeaders();
        }
        
        public void LoadMethods(DelegateFinder_Ok finder_ok, DelegateFinder_DoWork finder_DoWork, DelegateFinder_RunWorkerCompleted finder_RunWorkerCompleted)
        {
            _finder_Ok = finder_ok;
            _finder_DoWork = finder_DoWork;
            _finder_RunWorkerCompleted = finder_RunWorkerCompleted;
            LoadFilters();            
        }

        private void LoadFilters()
        {
            cmbType.Items.Clear();
            cmbType.Items.AddRange(_filterTypes.ToArray());
            _filters = new Dictionary<string, string>();
            foreach(string temp in _filterTypes)
            {
                _filters.Add(temp, "");
            }
            
        }

        #region Properties...


        public object Result
        {
            get { return _result; }
        }

        public List<string> FilterTypes { set => _filterTypes = value; }
        public Dictionary<string, string> Filters { get => _filters; set => _filters = value; }
        public string StrResult { get => _strResult;}


        #endregion

        private void frmFinder_Load(object sender, EventArgs e)
        {
            EnablingStages(1);
       
        }

        private void LoadHeaders()
        {
            lstItems.Columns.AddRange(_myHeaders.ToArray());            
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
            
        private void btnFind_Click(object sender, EventArgs e)
        {
            pbWorking.Visible = true;
            EnablingStages(2);
            bgwSearch.RunWorkerAsync(_filters);
        }

        private void bgwSearch_DoWork(object sender, DoWorkEventArgs e)
        {
            if (_finder_DoWork != null)
            {
                
                _finder_DoWork(e);
            }
            else
                MessageBox.Show(_strWrongConfig, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void EnablingStages(int stage)
        {
            switch(stage)
            {
                case 1:
                    btnCancel.Enabled = btnFind.Enabled = txtValue.Enabled = cmbType.Enabled = true;
                    btnOk.Enabled = lstItems.Enabled = false;
                    break;
                case 2:
                    btnCancel.Enabled =   true;
                    txtValue.Enabled = cmbType.Enabled = btnFind.Enabled = btnOk.Enabled = lstItems.Enabled = false;
                    break;
                case 3:
                    btnOk.Enabled = lstItems.Enabled = btnCancel.Enabled =  true;
                    btnFind.Enabled = txtValue.Enabled = cmbType.Enabled = true;
                    break;
            }
        }

        private void bgwSearch_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            _finder_RunWorkerCompleted(out _items,e);
            
            
            List<ListViewItem> data = new List<ListViewItem>(); 
            foreach (string[] item in _items)
            {
                data.Add(new ListViewItem(item, -1));
            }
            lstItems.Items.AddRange(data.ToArray());

            EnablingStages(3);
            pbWorking.Visible = false;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            List<string> selected = new List<string>();
            if (lstItems.SelectedItems.Count > 0)
            { 
                selected.Add(lstItems.SelectedItems[0].Text);
                for(int i=0; i<lstItems.SelectedItems[0].SubItems.Count; i++)
                {
                    selected.Add(lstItems.SelectedItems[0].SubItems[i].Text);
                }
                                
            }
            if (_finder_Ok != null)
            {
                _result = _finder_Ok(selected.ToArray());
                _strResult = selected.FirstOrDefault();
            }
            else
                MessageBox.Show(_strWrongConfig, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            this.Close();
        }

    

        private void lstItems_DoubleClick(object sender, EventArgs e)
        {
            btnOk_Click(sender, e);
        }

        private void cmbType_TextChanged(object sender, EventArgs e)
        {
            if(!string.IsNullOrWhiteSpace(cmbType.Text)&& _filters.ContainsKey(cmbType.Text))
                txtValue.Text = _filters[cmbType.Text];
        }

        private void txtValue_LostFocus(object sender, EventArgs e)
        {
            _filters[cmbType.Text] = txtValue.Text;
        }
    }
}
